package com.nurfaizin.finalkade.view.ui.teamoverview

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nurfaizin.finalkade.R
import kotlinx.android.synthetic.main.fragment_overview.*

class TeamOverViewFragment : Fragment(){

    private var teamOverview: String? = null

    companion object {

        private const val ARG_TEAM_OVERVIEW = "teamOverview"

        @JvmStatic
        fun newInstance(teamOverview: String) =
            TeamOverViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TEAM_OVERVIEW, teamOverview)
                }
            }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            teamOverview = it.getString(ARG_TEAM_OVERVIEW)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_overview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        teamOverview?.let { setTeamOverview(teamOverview ?: "") }
    }

    private fun setTeamOverview(teamOverview: String) {
        team_overview_txt_overview.text = teamOverview
    }
}