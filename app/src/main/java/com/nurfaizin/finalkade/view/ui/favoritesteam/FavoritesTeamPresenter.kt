package com.nurfaizin.finalkade.view.ui.favoritesteam

import android.content.Context
import android.os.Handler
import com.nurfaizin.finalkade.model.Team
import com.nurfaizin.finalkade.utils.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavoritesTeamPresenter(private val view : FavoritesTeamContract.View,
                             private val context: Context) : FavoritesTeamContract.Presenter{


    override fun loadFavoriteTeams() {
        view.loading(true)

        context.database.use {
            val result = select(Team.TABLE_FAVORITE_TEAM)
            val favoriteTeams = result.parseList(classParser<Team>())

            Handler().postDelayed({
                view.loading(false)
                view.setFavoriteTeams(favoriteTeams)
            }, 800)
        }
    }

}