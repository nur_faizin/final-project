package com.nurfaizin.finalkade.view.ui.teamsearch

import com.nurfaizin.finalkade.model.Team

interface TeamSearchContract{

    interface View {

        fun loading(loading: Boolean)

        fun setTeamsSearchResult(teams: List<Team>)

        fun setNull()

        fun showToastMsg(msg: String)
    }

    interface Presenter {

        fun clearComposite()

        fun disposeComposite()

        fun searchTeam(query: String)
    }
}