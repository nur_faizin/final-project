package com.nurfaizin.finalkade.model

data class TeamResponse(val teams : List<Team>)