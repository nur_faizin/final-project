package com.nurfaizin.finalkade.view.ui.match

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.adapter.ViewPagerAdapter
import com.nurfaizin.finalkade.view.ui.matchsearch.MatchSearchActivity
import kotlinx.android.synthetic.main.fragment_matches.*
import org.jetbrains.anko.support.v4.startActivity

class MatchFragment : Fragment(){

    private val tabTitles = arrayOf<CharSequence>("Next", "Last")
    private val totalTabs = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_matches,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ViewPagerAdapter(fragmentManager!!, tabTitles, totalTabs)
        matches_home_viewpager.adapter = adapter
        matches_home_tab_layout.setupWithViewPager(matches_home_viewpager)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.matches_home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.matches_home_menu_search -> {
                startActivity<MatchSearchActivity>()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}