package com.nurfaizin.finalkade.view.ui.favorites

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nurfaizin.finalkade.view.ui.favoritesmatch.FavoritesMatchFragment
import com.nurfaizin.finalkade.view.ui.favoritesteam.FavoritesTeamFragment

class FavoritesViewPager(fm: FragmentManager,
                         private var tabTitles: Array<CharSequence>,
                         private var totalTabs: Int) : FragmentStatePagerAdapter(fm){
    override fun getCount(): Int {
        return totalTabs
    }

    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> FavoritesMatchFragment()
            1 -> FavoritesTeamFragment()
            else -> FavoritesMatchFragment()
        }
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }


}