package com.nurfaizin.finalkade.model

data class LeagueResponse(val leagues : List<League>)