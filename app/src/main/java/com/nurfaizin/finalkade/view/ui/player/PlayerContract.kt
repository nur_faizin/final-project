package com.nurfaizin.finalkade.view.ui.player

import com.nurfaizin.finalkade.model.Player

interface PlayerContract{


    interface View {
        fun loadingData(loading: Boolean)

        fun setPlayers(players: List<Player>)

        fun showToastMsg(msg: String)
    }

    interface Presenter {

        fun clearComposite()

        fun disposeComposite()

        fun loadPlayersData(idTeam: String)
    }
}