package com.nurfaizin.finalkade


import com.nurfaizin.finalkade.api.RetrofitClient
import com.nurfaizin.finalkade.api.TheSportDBApi

object ApiServices {

    fun getTheSportDBApiServices(): TheSportDBApi{
        return RetrofitClient
                .getClient(BuildConfig.TSDB_BASE_URL)
                .create(TheSportDBApi::class.java)
    }
}