package com.nurfaizin.finalkade.view.ui.match

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.utils.MyDateFormat
import com.nurfaizin.finalkade.utils.gone
import com.nurfaizin.finalkade.utils.visible
import kotlinx.android.synthetic.main.listmatch.view.*

class MatchAdapter(private val matchEvents: List<MatchEvent>,
                        private val alarmEnabled: Boolean,
                        private val onItemClickListener: (idMatch: String) -> Unit,
                        private val onAlarmClickListener: (matchEvent: MatchEvent) -> Unit)
    : RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(context).inflate(R.layout.listmatch, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(matchEvents[position],alarmEnabled, onItemClickListener, onAlarmClickListener)
    }

    override fun getItemCount(): Int = matchEvents.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private val imgAddToCalendar: ImageView = itemView.adapter_match_event_img_add_to_calendar
        private val txtDate: TextView = itemView.adapter_match_event_txt_date
        private val txtTime: TextView = itemView.adapter_match_event_txt_time
        private val txtHomeTeam: TextView = itemView.adapter_match_event_txt_home_team
        private val txtAwayTeam: TextView = itemView.adapter_match_event_txt_away_team
        private val txtHomeScore: TextView = itemView.adapter_match_event_txt_home_score
        private val txtAwayScore: TextView = itemView.adapter_match_event_txt_away_score

        fun bindItem(matchEvent: MatchEvent,
                     alarmEnabled: Boolean,
                     onItemClickListener: (idMatch: String) -> Unit,
                     onAlarmClickListener: (matchEvent: MatchEvent) -> Unit) {
            val date = matchEvent.dateEvent ?: ""
            if (date.isNotEmpty()) txtDate.text = MyDateFormat.dateEn(date)


            val time = matchEvent.strTime ?: ""
            if (time.isNotEmpty()) txtTime.text = MyDateFormat.time(time)

            val homeTeam = matchEvent.strHomeTeam ?: ""
            txtHomeTeam.text = homeTeam


            val awayTeam = matchEvent.strAwayTeam ?: ""
            txtAwayTeam.text = awayTeam


            val homeScore = matchEvent.intHomeScore ?: ""
            txtHomeScore.text = homeScore


            val awayScore = matchEvent.intAwayScore ?: ""
            txtAwayScore.text = awayScore

            itemView.setOnClickListener { onItemClickListener(matchEvent.idEvent ?: "") }


            if (alarmEnabled) {
                imgAddToCalendar.visible()
                imgAddToCalendar.setOnClickListener {
                    onAlarmClickListener(matchEvent)
                }
            } else {
                imgAddToCalendar.gone()
            }
        }
    }
}