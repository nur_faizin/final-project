package com.nurfaizin.finalkade.view.ui.team

import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.LeagueResponse
import com.nurfaizin.finalkade.model.TeamResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TeamPresenter(private val view :TeamContract.View,
                    private val theSportDBApi: TheSportDBApi):TeamContract.Presenter{

    private val composite = CompositeDisposable()

    override fun clearComposite() {
       composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }

    override fun loadSpinnerData() {
        view.loadingSpinner(true)
        composite.add(
            theSportDBApi.getListLeague()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({leagueResponse: LeagueResponse? ->
                    view.loadingSpinner(false)
                    leagueResponse?.leagues?.let { leagues ->
                        view.setSpinnerData(leagues)
                    }
                },{
                    view.loadingSpinner(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

    override fun getTeam(idLeague: String) {
        view.loadingTeamsData(true)

        composite.add(
            theSportDBApi.getTeamsInLeague(idLeague)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({teamResponse: TeamResponse? ->
                    view.loadingTeamsData(false)
                    teamResponse?.teams?.let {teams ->
                        view.setTeams(teams)
                    }
                }, {
                    view.loadingTeamsData(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

}