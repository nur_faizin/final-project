package com.nurfaizin.finalkade.view.ui.matchsearch

import com.nurfaizin.finalkade.model.MatchEvent

interface MatchSearchContract{

    interface View {

        fun loading(loading: Boolean)

        fun setMatchesSearchResult(matchEvents: List<MatchEvent>)

        fun setNull()

        fun showToastMsg(msg: String)
    }

    interface Presenter {

        fun clearComposite()

        fun disposeComposite()

        fun searchMatch(query: String)
    }
}