package com.nurfaizin.finalkade.view.ui.matchnext

import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.MatchEvent

interface MatchNextContract{
    interface View {

        fun loadingSpinnerData(loading: Boolean)

        fun setSpinnerData(leagues: List<League>)

        fun loadingPrevMatchesData(loading: Boolean)

        fun setMatchEvents(matchEvents: List<MatchEvent>)

        fun showToastMsg(msg: String)
    }

    interface Presenter {

        fun clearComposite()

        fun disposeComposite()

        fun loadLeague()

        fun loadNextMatchEvents(idLeague: String)
    }
}