package com.nurfaizin.finalkade.view.ui.favoritesmatch

import com.nurfaizin.finalkade.model.MatchEvent

interface FavoritesMatchContract{

    interface View {
        fun loading(loading: Boolean)

        fun setFavoriteMatches(matches: List<MatchEvent>)
    }

    interface Presenter {
        fun loadFavoriteMatches()
    }
}