package com.nurfaizin.finalkade.utils

import com.nurfaizin.finalkade.utils.MyDateFormat.dateEn
import com.nurfaizin.finalkade.utils.MyDateFormat.gregorianDateTimeInMillis
import com.nurfaizin.finalkade.utils.MyDateFormat.time
import org.junit.Assert
import org.junit.Test

@Test
fun testDateEn() {
    Assert.assertEquals("Sat, 20 Oct 2018", dateEn("2018-10-20"))
}

@Test
fun testTime() {
    Assert.assertEquals("10:30", time("10:30:00+00:00"))
}

@Test
fun testGregorianDateTimeInMillis() {
    Assert.assertEquals(
        1540018800000L,
        gregorianDateTimeInMillis("2018-10-20", "14:00:00+00:00")
    )
}