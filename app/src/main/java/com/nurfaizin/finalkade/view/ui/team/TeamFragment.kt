package com.nurfaizin.finalkade.view.ui.team

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.Team
import com.nurfaizin.finalkade.utils.gone
import com.nurfaizin.finalkade.utils.visible
import com.nurfaizin.finalkade.view.ui.teamdetail.TeamDetail
import com.nurfaizin.finalkade.view.ui.teamsearch.TeamSearchActivity
import kotlinx.android.synthetic.main.fragment_team.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity

class TeamFragment : Fragment(),TeamContract.View{


    private lateinit var presenter: TeamPresenter
    private var load = false
    private val leagues: MutableList<League> = mutableListOf()
    private lateinit var adapterRvTeams: RecyclerView.Adapter<*>
    private var teams: MutableList<Team> = mutableListOf()
    private var idLeague = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter = TeamPresenter(this,ApiServices.getTheSportDBApiServices())
        presenter.loadSpinnerData()
        teams_home_swipe_refresh.setOnRefreshListener {
            if (!load) {
                if (idLeague.isNotEmpty()) {
                    presenter.getTeam(idLeague)
                } else {
                    teams_home_swipe_refresh.isRefreshing = false
                }
            } else {
                teams_home_swipe_refresh.isRefreshing = false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.team_home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.team_home_menu_search -> {
                startActivity<TeamSearchActivity>()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun loadingSpinner(loading: Boolean) {
        load = loading

        if (loading) {
            progressBar?.visible()
        } else {
            progressBar?.gone()
        }
    }

    override fun setSpinnerData(leagues: List<League>) {
        this.leagues.clear()
        this.leagues.addAll(leagues)

        val spinnerItems = arrayOfNulls<String>(leagues.size)
        for (i in leagues.indices) {
            spinnerItems[i] = leagues[i].name
        }

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                idLeague = leagues[position].id ?: ""
                if (idLeague.isNotEmpty()) presenter.getTeam(idLeague)
            }

        }

    }

    override fun loadingTeamsData(loading: Boolean) {
        load = loading
        teams_home_swipe_refresh?.isRefreshing = loading
    }

    override fun setTeams(teams: List<Team>) {
        this.teams.clear()
        this.teams.addAll(teams)
        adapterRvTeams.notifyDataSetChanged()
    }

    override fun showToastMsg(msg: String) {
       longToast(msg)
    }


    private fun initLayout(){
        adapterRvTeams = TeamAdapter(teams) {
            team ->
            startActivity<TeamDetail>(TeamDetail.INTENT_TEAM to team)

        }
        rv.layoutManager = LinearLayoutManager(ctx)
        rv.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        rv.adapter = adapterRvTeams
    }


}