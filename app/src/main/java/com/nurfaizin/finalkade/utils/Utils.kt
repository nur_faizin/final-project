package com.nurfaizin.finalkade.utils

import android.content.Context
import android.view.View
import com.nurfaizin.finalkade.db.DatabaseHelper

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}


val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)