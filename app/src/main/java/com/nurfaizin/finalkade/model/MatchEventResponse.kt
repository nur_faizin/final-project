package com.nurfaizin.finalkade.model

data class MatchEventResponse(val events : List<MatchEvent>)