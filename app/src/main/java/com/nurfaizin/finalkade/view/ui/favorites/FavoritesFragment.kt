package com.nurfaizin.finalkade.view.ui.favorites

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nurfaizin.finalkade.R
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment : Fragment(){
    private val tabTitles = arrayOf<CharSequence>("Match", "Team")
    private val totalTabs = 2
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = FavoritesViewPager(fragmentManager!!, tabTitles, totalTabs)
        favorites_viewpager.adapter = adapter
        favorites_tab.setupWithViewPager(favorites_viewpager)
    }
}