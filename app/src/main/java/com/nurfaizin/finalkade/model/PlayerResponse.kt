package com.nurfaizin.finalkade.model

data class PlayerResponse(val player: List<Player>)