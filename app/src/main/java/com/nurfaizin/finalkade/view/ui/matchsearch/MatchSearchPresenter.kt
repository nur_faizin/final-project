package com.nurfaizin.finalkade.view.ui.matchsearch

import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.SearchMatchEventResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MatchSearchPresenter(private val view : MatchSearchContract.View,
                           private val theSportDBApi: TheSportDBApi) : MatchSearchContract.Presenter{

    private val composite = CompositeDisposable()

    override fun clearComposite() {
        composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }

    override fun searchMatch(query: String) {
        view.loading(true)

        composite.add(
            theSportDBApi.getSearchEvents(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({searchMatchEventResponse: SearchMatchEventResponse? ->
                    view.loading(false)
                    searchMatchEventResponse?.matchEvents?.let {matchEvents ->
                        if (matchEvents.isNotEmpty()) {
                            view.setMatchesSearchResult(matchEvents)
                        } else {
                            view.setNull()
                        }
                    } ?: view.setNull()
                }, {
                    view.loading(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

}