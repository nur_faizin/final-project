package com.nurfaizin.finalkade.view.ui.teamdetail

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import com.nurfaizin.finalkade.model.Team
import com.nurfaizin.finalkade.utils.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select


class TeamDetailPresenter(private val view : TeamDetailContract.View,
                          private val context: Context): TeamDetailContract.Presenter{


    override fun teamFavoriteState(idTeam: String) {

        context.database.use {
            val result = select(Team.TABLE_FAVORITE_TEAM)
                .whereArgs("(${Team.TEAM_ID} = {teamId})",
                    "teamId" to idTeam)
            val favorite = result.parseList(classParser<Team>())
            if (!favorite.isEmpty()) {
                view.setTeamFavoriteState(true)
            }
        }
    }

    override fun addToFavorite(team: Team) {
        try{
            context.database.use {
                insert(Team.TABLE_FAVORITE_TEAM,
                    Team.TEAM_ID to team.teamId,
                    Team.TEAM_NAME to team.teamName,
                    Team.TEAM_BADGE to team.teamBadge,
                    Team.INT_FORMED_YEAR to team.intFormedYear,
                    Team.STR_STADIUM to team.strStadium,
                    Team.STR_DESCRIPTION_EN to team.strDescriptionEN)
            }
            view.onAddToFavoriteSuccess("Added to favorites")
        }catch (e : SQLiteException){
            view.onAddToFavoriteFailed(e.localizedMessage)
        }
    }

    override fun removeFromFavorite(idTeam: String) {
        try {
            context.database.use {
                delete(Team.TABLE_FAVORITE_TEAM, "(${Team.TEAM_ID} = {idTeam})",
                    "idTeam" to idTeam)
            }
            view.onRemoveFromFavoriteSuccess("Removed from favorite")
        } catch (e: SQLiteConstraintException){
            view.onRemoveFromFavoriteFailed(e.localizedMessage)
        }
    }

}