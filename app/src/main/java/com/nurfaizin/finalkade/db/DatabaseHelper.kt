package com.nurfaizin.finalkade.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.model.Team
import org.jetbrains.anko.db.*

class DatabaseHelper(context : Context): ManagedSQLiteOpenHelper(context,"Favorites.db",null,1){

    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(context: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(context.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */

    override fun onCreate(db: SQLiteDatabase?) {

        db?.createTable(Team.TABLE_FAVORITE_TEAM, true,
            Team.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Team.TEAM_ID to TEXT + UNIQUE,
            Team.TEAM_NAME to TEXT,
            Team.TEAM_BADGE to TEXT,
            Team.INT_FORMED_YEAR to TEXT,
            Team.STR_STADIUM to TEXT,
            Team.STR_DESCRIPTION_EN to TEXT
        )

        db?.createTable(MatchEvent.TABLE_FAVORITE_MATCH, true,
            MatchEvent.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            MatchEvent.ID_EVENT to TEXT + UNIQUE,
            MatchEvent.STR_HOME_TEAM to TEXT,
            MatchEvent.STR_AWAY_TEAM to TEXT,
            MatchEvent.INT_HOME_SCORE to TEXT,
            MatchEvent.INT_AWAY_SCORE to TEXT,
            MatchEvent.STR_FILENAME to TEXT,
            MatchEvent.DATE_EVENT to TEXT,
            MatchEvent.STR_TIME to TEXT
        )
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     *
     *
     *
     * The SQLite ALTER TABLE documentation can be found
     * [here](http://sqlite.org/lang_altertable.html). If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     *
     *
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     *
     *
     * @param db The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(Team.TABLE_FAVORITE_TEAM, true)
        db?.dropTable(MatchEvent.TABLE_FAVORITE_MATCH, true)
    }

}