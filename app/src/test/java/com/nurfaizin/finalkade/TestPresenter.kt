package com.nurfaizin.finalkade

import com.nurfaizin.finalkade.utils.SchedulersRule
import org.junit.ClassRule

open class TestPresenter {

    companion object {
        @ClassRule
        @JvmField
        val schedulers = SchedulersRule()
    }
}