package com.nurfaizin.finalkade.view.ui.detailmatch

import android.content.Context
import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.model.MatchResponse
import com.nurfaizin.finalkade.model.TeamResponse
import com.nurfaizin.finalkade.utils.database
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import java.sql.SQLClientInfoException

class DetailMatchPresenter(private val view : DetailMatchContract.View,
                           private val context : Context,
                           private val theSportDBApi: TheSportDBApi) : DetailMatchContract.Presenter{



    private val composite = CompositeDisposable()

    override fun clearComposite() {
        composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }


    override fun loadMatch(idMatch: String) {
        view.load(true)
        composite.add(
            theSportDBApi.getEventDetails(idMatch)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({matchResponse: MatchResponse? ->
                    view.load(false)
                    matchResponse?.events?.let { matches->
                        if(matches.isNotEmpty()){
                            view.setMatch(matches[0])
                        }else{
                            view.showMsg("get data failed or no internet access")
                        }
                    }
                },{
                    view.load(false)
                    view.showMsg("${it.message}")
                })
        )
    }

    override fun loadHomeTeam(idHomeTeam: String) {
        view.load(true)
        composite.add(
            theSportDBApi.getTeam(idHomeTeam)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({teamResponse: TeamResponse? ->
                    view.load(false)
                    teamResponse?.teams?.let { teams->
                        if(teams.isNotEmpty()){
                            val teamBadge = teams[0].teamBadge?:""
                            if (teamBadge.isNotEmpty()){
                                view.setHomeBadge(teamBadge)
                            }else{
                                view.showMsg("get team badge failed")
                            }
                        }else{
                            view.showMsg("get data failed")
                        }
                    }
                },{
                    view.load(false)
                    view.showMsg("${it.message}")
                })
        )

    }

    override fun loadAwayTeam(idAwayTeam: String) {
        view.load(true)
        composite.add(
            theSportDBApi.getTeam(idAwayTeam)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({teamResponse: TeamResponse? ->
                    view.load(false)
                    teamResponse?.teams?.let { teams->
                        if(teams.isNotEmpty()){
                            val teamBadge = teams[0].teamBadge?:""
                            if (teamBadge.isNotEmpty()){
                                view.setHomeBadge(teamBadge)
                            }else{
                                view.showMsg("get team badge failed")
                            }
                        }else{
                            view.showMsg("get data failed")
                        }
                    }
                },{
                    view.load(false)
                    view.showMsg("${it.message}")
                })
        )
    }

    override fun matchFavoriteState(idEvent: String) {
       context.database.use {
           val result = select(MatchEvent.TABLE_FAVORITE_MATCH)
               .whereArgs("(${MatchEvent.ID_EVENT} = {idEvent})",
                   "idEvent" to idEvent)
           val favorite = result.parseList(classParser<MatchEvent>())
           if (!favorite.isEmpty()) {
               view.setFavoriteState(true)
           }
       }
    }

    override fun addToFavorite(matchEvent: MatchEvent) {
       try {
           context.database.use {
               insert(MatchEvent.TABLE_FAVORITE_MATCH,
                   MatchEvent.ID_EVENT to matchEvent.idEvent,
                   MatchEvent.STR_HOME_TEAM to matchEvent.strHomeTeam,
                   MatchEvent.STR_AWAY_TEAM to matchEvent.strAwayTeam,
                   MatchEvent.INT_HOME_SCORE to matchEvent.intHomeScore,
                   MatchEvent.INT_AWAY_SCORE to matchEvent.intAwayScore,
                   MatchEvent.STR_FILENAME to matchEvent.strFilename,
                   MatchEvent.DATE_EVENT to matchEvent.dateEvent,
                   MatchEvent.STR_TIME to matchEvent.strTime
               )
           }
           view.addFavoriteSuccess("Added to favorites")
       }catch (e : SQLClientInfoException){
           view.addFavorieFailed(e.localizedMessage)
       }
    }

    override fun removeFromFavorite(idEvent: String) {
       try {
           context.database.use {
               delete(MatchEvent.TABLE_FAVORITE_MATCH,
                   "(${MatchEvent.ID_EVENT} = {idEvent})",
                   "idEvent" to idEvent)
           }
           view.removeFavoriteSuccess("Removed from favorite")
           }catch (e : SQLClientInfoException){
           view.removeFavoriteFailed(e.localizedMessage)

       }
    }

}