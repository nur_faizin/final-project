package com.nurfaizin.finalkade.view.ui.playerdetail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Player
import kotlinx.android.synthetic.main.player_detail.*

class PlayerDetail : AppCompatActivity(){

    private lateinit var player: Player
    companion object {
        const val INTENT_PLAYER = "player"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.player_detail)
        setTitle(R.string.player_details_page_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.hasExtra(INTENT_PLAYER)) {
            player = intent.getParcelableExtra(INTENT_PLAYER)
            setPlayerDetails(player)
        }
    }

    private fun setPlayerDetails(player: Player) {

        val name = player.strPlayer ?: ""
        title = name

        val fanArt = player.strFanart1 ?: ""
        if (fanArt.isNotEmpty()) Glide.with(this).load(fanArt).into(player_img)

        val weight = player.strWeight ?: ""
        player_details_txt_weight.text = weight

        val height = player.strHeight?: ""
        player_details_txt_height.text = height

        val position = player.strPosition ?: ""
        player_details_txt_position.text = position

        val overview = player.strDescriptionEN ?: ""
        player_details_txt_overview.text = overview
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}