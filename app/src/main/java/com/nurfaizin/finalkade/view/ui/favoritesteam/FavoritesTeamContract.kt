package com.nurfaizin.finalkade.view.ui.favoritesteam

import com.nurfaizin.finalkade.model.Team

interface FavoritesTeamContract{


    interface View {
        fun loading(loading: Boolean)

        fun setFavoriteTeams(teams: List<Team>)
    }

    interface Presenter {
        fun loadFavoriteTeams()
    }
}