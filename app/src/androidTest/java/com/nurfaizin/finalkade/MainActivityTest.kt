package com.nurfaizin.finalkade

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.runner.AndroidJUnit4
import com.nurfaizin.finalkade.view.MainActivity
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import com.nurfaizin.finalkade.R.id.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest{

    @Rule
    @JvmField
    val mainRule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun testOnClickBottomNavigation() {
        onView(ViewMatchers.withId(dashboard_bot_nav_matches)).check(ViewAssertions.matches(ViewMatchers.isClickable()))
        onView(ViewMatchers.withId(dashboard_bot_nav_teams)).check(ViewAssertions.matches(ViewMatchers.isClickable()))
        onView(ViewMatchers.withId(dashboard_bot_nav_favorites)).check(ViewAssertions.matches(ViewMatchers.isClickable()))
    }

    @Test
    fun testIsMatchesPageDisplayed() {
        onView(ViewMatchers.withId(dashboard_bot_nav_matches)).perform(ViewActions.click())
        onView(ViewMatchers.withId(matches_home_linear_main_layout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testIsTeamsPageDisplayed() {
        onView(ViewMatchers.withId(dashboard_bot_nav_teams)).perform(ViewActions.click())
        onView(ViewMatchers.withId(teams_home_coordinator_main_layout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testIsFavoritesPageDisplayed() {
        onView(ViewMatchers.withId(dashboard_bot_nav_favorites)).perform(ViewActions.click())
        onView(ViewMatchers.withId(favorite_home_linear_main_layout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}