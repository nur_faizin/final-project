package com.nurfaizin.finalkade.view.ui.team

import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.TestPresenter
import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.LeagueResponse
import com.nurfaizin.finalkade.model.Team
import com.nurfaizin.finalkade.model.TeamResponse
import io.reactivex.Observable
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamPresenterTest : TestPresenter() {

    @Mock
    private lateinit var view: TeamContract.View

    @Mock
    private var theSportDBApiServices = ApiServices.getTheSportDBApiServices()

    private lateinit var presenter: TeamPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = TeamPresenter(view, theSportDBApiServices)
    }

    @Test
    fun loadSpinnerData() {
        val leagues = mutableListOf<League>().apply {
            add(League())
        }
        val leaguesResponse = LeagueResponse(leagues)
        Mockito.`when`(theSportDBApiServices.getListLeague())
            .thenReturn(Observable.just(leaguesResponse))

        val inOrder = Mockito.inOrder(view)
        presenter.loadSpinnerData()
        inOrder.verify(view).loadingSpinner(true)
        inOrder.verify(view).loadingSpinner(false)
        inOrder.verify(view).setSpinnerData(leagues)
    }

    @Test
    fun getTeam() {
        val teams = mutableListOf<Team>().apply {
            add(Team())
        }
        val teamResponse = TeamResponse(teams)
        Mockito.`when`(theSportDBApiServices.getTeamsInLeague("4332"))
            .thenReturn(Observable.just(teamResponse))

        val inOrder = Mockito.inOrder(view)
        presenter.getTeam("4332")
        inOrder.verify(view).loadingTeamsData(true)
        inOrder.verify(view).loadingTeamsData(false)
        inOrder.verify(view).setTeams(teams)
    }
}