package com.nurfaizin.finalkade

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.nurfaizin.finalkade.view.MainActivity
import com.nurfaizin.finalkade.view.ui.teamdetail.TeamDetail
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class TeamFragmentTest {

    @Rule
    @JvmField
    val mainRule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun testRvTeamsBehaviour() {
        Espresso.onView(ViewMatchers.withId(R.id.dashboard_bot_nav_teams)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.teams_home_coordinator_main_layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Thread.sleep(5000)
        Espresso.onView(ViewMatchers.withId(R.id.rv))
            .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
        Espresso.onView(ViewMatchers.withId(R.id.rv))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(4))
        Espresso.onView(ViewMatchers.withId(R.id.rv))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(4, ViewActions.click()))
        Intents.intended(IntentMatchers.hasComponent(TeamDetail::class.java.name))
    }
}