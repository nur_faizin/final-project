package com.nurfaizin.finalkade.view.ui.player

import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.TestPresenter
import com.nurfaizin.finalkade.model.Player
import com.nurfaizin.finalkade.model.PlayerResponse
import io.reactivex.Observable
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PlayerPresenterTest : TestPresenter() {

    @Mock
    private lateinit var view: PlayerContract.View

    @Mock
    private var theSportDBApiServices = ApiServices.getTheSportDBApiServices()

    private lateinit var presenter: PlayerPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = PlayerPresenter(view, theSportDBApiServices)
    }
    @Test
    fun loadPlayersData() {
        val players = mutableListOf<Player>().apply {
            add(Player())
        }
        val playerResponse = PlayerResponse(players)
        Mockito.`when`(theSportDBApiServices.getPLayersInTeam("133604"))
            .thenReturn(Observable.just(playerResponse))

        val inOrder = Mockito.inOrder(view)
        presenter.loadPlayersData("133604")
        inOrder.verify(view).loadingData(true)
        inOrder.verify(view).loadingData(false)
        inOrder.verify(view).setPlayers(players)
    }
}