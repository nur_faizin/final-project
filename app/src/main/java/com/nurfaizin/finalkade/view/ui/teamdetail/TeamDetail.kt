package com.nurfaizin.finalkade.view.ui.teamdetail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Team
import kotlinx.android.synthetic.main.team_detail.*
import org.jetbrains.anko.design.snackbar

class TeamDetail: AppCompatActivity(),TeamDetailContract.View{



    private val tabTitles = arrayOf<CharSequence>("Overview", "Players")
    private val totalTabs = 2
    private lateinit var presenter: TeamDetailPresenter
    private var team: Team? = null
    private var idTeam = ""
    private var teamOverview = ""
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false


    companion object {
        const val INTENT_TEAM = "team"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.team_detail)
        title = null
        setSupportActionBar(team_details_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        team_details_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        var adapter = TeamViewPager(supportFragmentManager, tabTitles, totalTabs,
            idTeam, teamOverview)
        presenter = TeamDetailPresenter(this,this)

        if (intent.hasExtra(INTENT_TEAM)) {
            team = intent.getParcelableExtra(INTENT_TEAM)
            team?.let {
                title = it.teamName ?: ""
                setTeamData(it)
                idTeam = it.teamId ?: ""
                teamOverview = it.strDescriptionEN ?: ""
                adapter = TeamViewPager(supportFragmentManager, tabTitles, totalTabs,
                    idTeam, teamOverview)
                presenter.teamFavoriteState(idTeam)
            }
        }
        team_details_view_pager.adapter = adapter
        team_details_tab_layout.setupWithViewPager(team_details_view_pager)
    }

    private fun setTeamData(team: Team) {

        val teamBadge = team.teamBadge ?: ""
        if (teamBadge.isNotEmpty()) {
            Glide.with(this).load(teamBadge).into(team_details_img_team_badge)
        }

        val formedYear = team.intFormedYear ?: ""
        team_details_txt_formed_year.text = formedYear


        val stadium = team.strStadium ?: ""
        team_details_txt_stadium.text = stadium
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.team_details_menu, menu)
        menuItem = menu
        if (isFavorite) {
            menuItem?.findItem(R.id.team_details_menu_favorite)?.setIcon(R.drawable.added_to_fav)
        }
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.team_details_menu_favorite-> {
                if (isFavorite) {
                    if (idTeam.isNotEmpty()) presenter.removeFromFavorite(idTeam)
                } else {
                    team?.let {
                        presenter.addToFavorite(it)
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setTeamFavoriteState(isFavorite: Boolean) {
        this.isFavorite = isFavorite
    }

    override fun onAddToFavoriteSuccess(msg: String) {
        snackbar(team_details_coordinator_layout, msg).show()
        isFavorite = true
        menuItem?.findItem(R.id.team_details_menu_favorite)?.setIcon(R.drawable.added_to_fav)
    }

    override fun onAddToFavoriteFailed(msg: String) {
        snackbar(team_details_coordinator_layout, msg).show()
    }

    override fun onRemoveFromFavoriteSuccess(msg: String) {
        snackbar(team_details_coordinator_layout, msg).show()
        isFavorite = false
        menuItem?.findItem(R.id.team_details_menu_favorite)?.setIcon(R.drawable.add_to_fav)
    }

    override fun onRemoveFromFavoriteFailed(msg: String) {
        snackbar(team_details_coordinator_layout, msg).show()
    }

}