package com.nurfaizin.finalkade.view.ui.matchprev


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.utils.gone
import com.nurfaizin.finalkade.utils.visible
import com.nurfaizin.finalkade.view.ui.detailmatch.DetailMatch
import com.nurfaizin.finalkade.view.ui.match.MatchAdapter
import kotlinx.android.synthetic.main.fragment_match_prev.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity


class MatchPrevFragment : Fragment(),MatchPrevContract.View{

    private lateinit var presenter: MatchPrevPresenter
    private var load = false
    private val leagues: MutableList<League> = mutableListOf()
    private lateinit var adapterRvMatchEvents: RecyclerView.Adapter<*>
    private var match: MutableList<MatchEvent> = mutableListOf()
    private var idLeague = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match_prev,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLayout()
        presenter = MatchPrevPresenter(this@MatchPrevFragment, ApiServices.getTheSportDBApiServices())
        presenter.loadLeague()
        swipeRefresh.setOnRefreshListener {
            if(!load){
                if(idLeague.isNotEmpty()){
                    presenter.loadPrevMatchEvents(idLeague)
                }else{
                    swipeRefresh.isRefreshing = false
                }
            }else{
                swipeRefresh.isRefreshing = false
            }

        }

    }

    private fun initLayout(){

        adapterRvMatchEvents = MatchAdapter(match,false,
            {idMatch ->
                startActivity<DetailMatch>(DetailMatch.INTENT_ID_MATCH to idMatch)

            },{

            })

        rv_before.layoutManager = LinearLayoutManager(ctx)
        rv_before.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        rv_before.adapter = adapterRvMatchEvents
    }

    override fun loadingSpinnerData(loading: Boolean) {
        load = loading
        if (load){
            progress_before?.visible()
        }else{
            progress_before?.gone()
        }
    }

    override fun setSpinnerData(leagues: List<League>) {
        this.leagues.clear()
        this.leagues.addAll(leagues)

        val spinnerItems = arrayOfNulls<String>(leagues.size)
        for (i in leagues.indices) {
            spinnerItems[i] = leagues[i].name
        }

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner_past.adapter = spinnerAdapter
        spinner_past.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                idLeague = leagues[position].id ?: ""
                if (idLeague.isNotEmpty())
                    presenter.loadPrevMatchEvents(idLeague)
            }

        }
    }

    override fun loadingPrevMatchesData(loading: Boolean) {
      load = loading
        swipeRefresh.isRefreshing = load
    }

    override fun setMatchEvents(matchEvents: List<MatchEvent>) {
        this.match.clear()
        this.match.addAll(matchEvents)
        adapterRvMatchEvents.notifyDataSetChanged()
    }

    override fun onStop() {
        presenter.clearComposite()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.disposeComposite()
        super.onDestroy()
    }

    override fun showToastMsg(msg: String) {
        longToast(msg)
    }

}