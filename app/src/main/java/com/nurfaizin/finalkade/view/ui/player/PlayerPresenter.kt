package com.nurfaizin.finalkade.view.ui.player

import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.PlayerResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PlayerPresenter(private val view : PlayerContract.View,
                      private val theSportDBApi: TheSportDBApi): PlayerContract.Presenter{

    private val composite = CompositeDisposable()

    override fun clearComposite() {
        composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }



    override fun loadPlayersData(idTeam: String) {
        view.loadingData(true)

        composite.add(
            theSportDBApi.getPLayersInTeam(idTeam)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({playerResponse: PlayerResponse? ->
                    view.loadingData(false)
                    playerResponse?.player?.let {players ->
                        if (players.isNotEmpty()) view.setPlayers(players)
                    }
                }, {
                    view.loadingData(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

}