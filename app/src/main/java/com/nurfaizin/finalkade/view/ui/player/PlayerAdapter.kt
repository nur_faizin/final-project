package com.nurfaizin.finalkade.view.ui.player

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Player
import kotlinx.android.synthetic.main.listplayer.view.*

class PlayerAdapter(private val player : List<Player>,
                    private val listener : (Player)->Unit) : RecyclerView.Adapter<PlayerAdapter.ViewHolder>(){
    private lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        context = p0.context
        val itemView = LayoutInflater.from(context).inflate(R.layout.listplayer, p0, false)
        return  ViewHolder(itemView)
    }

    override fun getItemCount()= player.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(context,player[p1],listener)
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        private val imgAvatar: ImageView = itemView.imgPlayer
        private val txtName: TextView = itemView.txtName
        private val txtPosition: TextView = itemView.txtPosition

        fun bindItem(mContext: Context, player: Player, onItemClickListener: (Player) -> Unit) {
            val avatar = player.strCutout ?: ""
            if (avatar.isNotEmpty()) Glide.with(mContext).load(avatar).into(imgAvatar)
            val name = player.strPlayer ?: ""
            txtName.text = name
            val position = player.strPosition ?: ""
            txtPosition.text = position
            itemView.setOnClickListener { onItemClickListener(player) }
        }
    }

}