package com.nurfaizin.finalkade.view.ui.team

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.*
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Team
import kotlinx.android.synthetic.main.list_team.view.*

class TeamAdapter(private val teams : List<Team>,
                  private val listener : (Team) -> Unit) :RecyclerView.Adapter<TeamAdapter.TeamViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): TeamViewHolder {
      context = parent.context
      return TeamViewHolder(from(context).inflate(R.layout.list_team, parent, false))
    }

    override fun getItemCount() = teams.size


    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItems(context,teams[position],listener)
    }


    class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){

        private val imgTeamBadge: ImageView = view.imgTeam
        private val txtTeamName: TextView = view.teamName

        fun bindItems(context: Context,teams : Team,listener: (Team) -> Unit){

            val teamBadge = teams.teamBadge ?:""
            val teamName = teams.teamName ?:""
            if(teamBadge.isNotEmpty())
                Glide.with(context).load(teamBadge).into(imgTeamBadge)

            txtTeamName.text = teamName

            itemView.setOnClickListener {
                listener(teams)
            }

        }

    }
}