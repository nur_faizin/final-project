package com.nurfaizin.finalkade.view.ui.player

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Player
import com.nurfaizin.finalkade.view.ui.playerdetail.PlayerDetail
import kotlinx.android.synthetic.main.fragment_player.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity

class PlayerFragment : Fragment() ,PlayerContract.View{

    companion object {
        private const val ARG_ID_TEAM = "idTeam"

        @JvmStatic
        fun newInstance(idTeam: String) =
            PlayerFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ID_TEAM, idTeam)
                }
            }
    }

    private var idTeam: String? = null
    private lateinit var presenter: PlayerPresenter
    private lateinit var adapterRvPlayers: RecyclerView.Adapter<*>
    private val players: MutableList<Player> = mutableListOf()
    private var loading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            idTeam = it.getString(ARG_ID_TEAM)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter = PlayerPresenter(this,ApiServices.getTheSportDBApiServices())
        idTeam?.let {
            if (it.isNotEmpty()) presenter.loadPlayersData(it)
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }
    private fun initLayout(){
        adapterRvPlayers = PlayerAdapter(players) {
           startActivity<PlayerDetail>(PlayerDetail.INTENT_PLAYER to it)
        }
        team_players_rv_matches.layoutManager = LinearLayoutManager(ctx)
        team_players_rv_matches.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        team_players_rv_matches.adapter = adapterRvPlayers
    }
    override fun loadingData(loading: Boolean) {
        this.loading = loading
        team_players_swipe_refresh.isRefreshing = loading
    }

    override fun setPlayers(players: List<Player>) {
        this.players.clear()
        this.players.addAll(players)
        adapterRvPlayers.notifyDataSetChanged()
    }

    override fun showToastMsg(msg: String) {
        longToast(msg)
    }

    override fun onStop() {
        presenter.clearComposite()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.disposeComposite()
        super.onDestroy()
    }

}