package com.nurfaizin.finalkade.view.ui.favoritesteam

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.Team
import com.nurfaizin.finalkade.view.ui.team.TeamAdapter
import com.nurfaizin.finalkade.view.ui.teamdetail.TeamDetail
import kotlinx.android.synthetic.main.fragment_favorites_team.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity

class FavoritesTeamFragment : Fragment(), FavoritesTeamContract.View{

    private lateinit var presenter: FavoritesTeamPresenter
    private var loading = false
    private lateinit var adapterRvTeams: RecyclerView.Adapter<*>
    private var teams: MutableList<Team> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_favorites_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter = FavoritesTeamPresenter(this, ctx)
        presenter.loadFavoriteTeams()
        favorite_team_swipe_refresh.setOnRefreshListener {
            if (!loading) {
                presenter.loadFavoriteTeams()
            } else {
                favorite_team_swipe_refresh.isRefreshing = false
            }
        }
    }

    private fun initLayout(){
        adapterRvTeams = TeamAdapter(teams) { team ->
            startActivity<TeamDetail>(TeamDetail.INTENT_TEAM to team)
        }
        favorite_team_rv_teams.layoutManager = LinearLayoutManager(ctx)
        favorite_team_rv_teams.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        favorite_team_rv_teams.adapter = adapterRvTeams
    }
    override fun loading(loading: Boolean) {
        this.loading = loading
        favorite_team_swipe_refresh?.isRefreshing = loading
    }

    override fun setFavoriteTeams(teams: List<Team>) {

        this.teams.clear()
        this.teams.addAll(teams)
        adapterRvTeams.notifyDataSetChanged()
    }

}