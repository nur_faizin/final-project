package com.nurfaizin.finalkade.model

data class MatchResponse(val events: List<Match>)