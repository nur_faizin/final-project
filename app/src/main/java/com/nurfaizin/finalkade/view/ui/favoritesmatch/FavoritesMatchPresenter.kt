package com.nurfaizin.finalkade.view.ui.favoritesmatch

import android.content.Context
import android.os.Handler
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.utils.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavoritesMatchPresenter(private val view : FavoritesMatchContract.View,
                              private val context: Context) : FavoritesMatchContract.Presenter{


    override fun loadFavoriteMatches() {
        view.loading(true)
        context.database.use {
            val result = select(MatchEvent.TABLE_FAVORITE_MATCH)
            val favoriteMatches = result.parseList(classParser<MatchEvent>())

            Handler().postDelayed({
                view.loading(false)
                view.setFavoriteMatches(favoriteMatches)
            }, 800)
        }
    }
}

