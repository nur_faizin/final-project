package com.nurfaizin.finalkade.view.ui.team

import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.Team

interface TeamContract{

    interface View {
        fun loadingSpinner(loading: Boolean)

        fun setSpinnerData(leagues: List<League>)

        fun loadingTeamsData(loading: Boolean)

        fun setTeams(teams: List<Team>)

        fun showToastMsg(msg: String)
    }

    interface Presenter {

        fun clearComposite()

        fun disposeComposite()

        fun loadSpinnerData()

        fun getTeam(idLeague: String)
    }
}