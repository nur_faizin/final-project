package com.nurfaizin.finalkade.view.ui.favoritesmatch

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.view.ui.detailmatch.DetailMatch
import com.nurfaizin.finalkade.view.ui.match.MatchAdapter
import kotlinx.android.synthetic.main.fragment_favorites_match.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity

class FavoritesMatchFragment : Fragment(),FavoritesMatchContract.View{

    private lateinit var presenter: FavoritesMatchPresenter
    private var loading = false
    private lateinit var adapterRvMatches: RecyclerView.Adapter<*>
    private var matches: MutableList<MatchEvent> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter = FavoritesMatchPresenter(this, ctx)
        presenter.loadFavoriteMatches()
        favorite_swiperefresh.setOnRefreshListener {
            if (!loading) {
                presenter.loadFavoriteMatches()
            } else {
                favorite_swiperefresh.isRefreshing = false
            }
        }

    }

    override fun loading(loading: Boolean) {
        this.loading = loading
        favorite_swiperefresh?.isRefreshing = loading
    }

    override fun setFavoriteMatches(matches: List<MatchEvent>) {
        this.matches.clear()
        this.matches.addAll(matches)
        adapterRvMatches.notifyDataSetChanged()
    }

    private fun initLayout() {
        adapterRvMatches = MatchAdapter(matches, false,
            // on item click
            {idMatch ->
                startActivity<DetailMatch>(DetailMatch.INTENT_ID_MATCH to idMatch)
            },
            // on alarm click
            {/*disabled*/}
        )
        rv_favorites.layoutManager = LinearLayoutManager(ctx)
        rv_favorites.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        rv_favorites.adapter = adapterRvMatches
    }

}