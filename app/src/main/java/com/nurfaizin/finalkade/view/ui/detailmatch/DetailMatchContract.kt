package com.nurfaizin.finalkade.view.ui.detailmatch

import com.nurfaizin.finalkade.model.Match
import com.nurfaizin.finalkade.model.MatchEvent

interface DetailMatchContract{

    interface View{
        fun load(loading : Boolean)
        fun setMatch(match:Match)
        fun setHomeBadge(teamBadge : String)
        fun setAwayBadge(teamBadge: String)
        fun showMsg(msg : String)
        fun setFavoriteState(isFavorite: Boolean)
        fun addFavoriteSuccess(msg: String)
        fun addFavorieFailed(msg: String)
        fun removeFavoriteSuccess(msg: String)
        fun removeFavoriteFailed(msg: String)
    }

    interface Presenter{
        fun clearComposite()

        fun disposeComposite()

        fun loadMatch(idMatch: String)

        fun loadHomeTeam(idHomeTeam: String)

        fun loadAwayTeam(idAwayTeam: String)

        fun matchFavoriteState(idEvent: String)

        fun addToFavorite(matchEvent: MatchEvent)

        fun removeFromFavorite(idEvent: String)
    }
}