package com.nurfaizin.finalkade.view.ui.teamsearch

import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.TeamResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TeamSearchPresenter(private val view : TeamSearchContract.View,
                          private val theSportDBApi: TheSportDBApi) : TeamSearchContract.Presenter{


    private val composite = CompositeDisposable()

    override fun clearComposite() {
        composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }

    override fun searchTeam(query: String) {
        view.loading(true)

        composite.add(
            theSportDBApi.searchTeam(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({teamResponse: TeamResponse? ->
                    view.loading(false)
                    teamResponse?.teams?.let {teams ->
                        if (teams.isNotEmpty()) {
                            view.setTeamsSearchResult(teams)
                        } else {
                            view.setNull()
                        }
                    } ?: view.setNull()
                }, {
                    view.loading(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

}