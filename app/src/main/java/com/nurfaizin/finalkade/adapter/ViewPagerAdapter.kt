package com.nurfaizin.finalkade.adapter


import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nurfaizin.finalkade.view.ui.matchnext.MatchNextFragment
import com.nurfaizin.finalkade.view.ui.matchprev.MatchPrevFragment

class ViewPagerAdapter(fm: FragmentManager,
                                 private var tabTitles: Array<CharSequence>,
                                 private var totalTabs: Int)
    : FragmentStatePagerAdapter(fm) {

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MatchNextFragment()
            1 -> MatchPrevFragment()
            else -> MatchPrevFragment()
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

}