package com.nurfaizin.finalkade.view.ui.teamdetail

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.nurfaizin.finalkade.view.ui.player.PlayerFragment
import com.nurfaizin.finalkade.view.ui.teamoverview.TeamOverViewFragment

class TeamViewPager(fm : FragmentManager,
                    private var tabTitles: Array<CharSequence>,
                    private var totalTabs: Int,
                    private var idTeam: String,
                    private var teamOverview: String):FragmentStatePagerAdapter(fm){


    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> TeamOverViewFragment.newInstance(teamOverview)
            1 -> PlayerFragment.newInstance(idTeam)
            else -> TeamOverViewFragment()
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

}