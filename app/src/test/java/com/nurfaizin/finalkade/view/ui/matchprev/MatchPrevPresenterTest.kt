package com.nurfaizin.finalkade.view.ui.matchprev

import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.TestPresenter
import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.LeagueResponse
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.model.MatchEventResponse
import io.reactivex.Observable
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MatchPrevPresenterTest : TestPresenter() {

    @Mock
    private lateinit var view: MatchPrevContract.View

    @Mock
    private var theSportDBApiServices = ApiServices.getTheSportDBApiServices()

    private lateinit var presenter: MatchPrevPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = MatchPrevPresenter(view, theSportDBApiServices)
    }

    @Test
    fun loadLeague() {
        val leagues = mutableListOf<League>().apply {
            add(League())
        }
        val leaguesResponse = LeagueResponse(leagues)
        Mockito.`when`(theSportDBApiServices.getListLeague())
            .thenReturn(Observable.just(leaguesResponse))

        val inOrder = Mockito.inOrder(view)
        presenter.loadLeague()
        inOrder.verify(view).loadingSpinnerData(true)
        inOrder.verify(view).loadingSpinnerData(false)
        inOrder.verify(view).setSpinnerData(leagues)
    }

    @Test
    fun loadPrevMatchEvents() {
        val events = mutableListOf<MatchEvent>().apply {
            add(MatchEvent())
        }
        val matchEventResponse = MatchEventResponse(events)
        Mockito.`when`(theSportDBApiServices.getNextMatchEvents("4332"))
            .thenReturn(Observable.just(matchEventResponse))

        val inOrder = Mockito.inOrder(view)
        presenter.loadPrevMatchEvents("4332")
        inOrder.verify(view).loadingPrevMatchesData(true)
        inOrder.verify(view).loadingPrevMatchesData(false)
        inOrder.verify(view).setMatchEvents(events)
    }
}