package com.nurfaizin.finalkade

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.runner.AndroidJUnit4
import com.nurfaizin.finalkade.view.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith



    @RunWith(AndroidJUnit4::class)
    class FavoriteMatchTest {

        @Rule
        @JvmField
        val mainRule = IntentsTestRule(MainActivity::class.java)

        @Test
        fun testRvFavMatchBehaviour() {
            Espresso.onView(ViewMatchers.withId(R.id.dashboard_bot_nav_favorites)).perform(ViewActions.click())
            Espresso.onView(ViewMatchers.withId(R.id.favorite_home_linear_main_layout))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            Espresso.onView(ViewMatchers.withId(R.id.favorites_viewpager)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            Espresso.onView(ViewMatchers.withId(R.id.rv_favorites))
        }


    }
