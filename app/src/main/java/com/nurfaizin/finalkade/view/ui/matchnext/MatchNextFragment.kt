package com.nurfaizin.finalkade.view.ui.matchnext

import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.nurfaizin.finalkade.ApiServices
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.model.League
import com.nurfaizin.finalkade.model.MatchEvent
import com.nurfaizin.finalkade.utils.MyDateFormat
import com.nurfaizin.finalkade.utils.gone
import com.nurfaizin.finalkade.utils.visible
import com.nurfaizin.finalkade.view.ui.detailmatch.DetailMatch
import com.nurfaizin.finalkade.view.ui.match.MatchAdapter
import kotlinx.android.synthetic.main.fragment_match_next.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity
import java.util.concurrent.TimeUnit

class MatchNextFragment : Fragment(), MatchNextContract.View{

    private lateinit var presenter: MatchNextPresenter
    private var load = false
    private val leagues: MutableList<League> = mutableListOf()
    private lateinit var adapterRvMatchEvents: RecyclerView.Adapter<*>
    private var matchEvents: MutableList<MatchEvent> = mutableListOf()
    private var idLeague = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_match_next, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter = MatchNextPresenter(this,ApiServices.getTheSportDBApiServices())
        presenter.loadLeague()
        next_matches_swipe_refresh.setOnRefreshListener {
            if (!load) {
                if (idLeague.isNotEmpty()) {
                    presenter.loadNextMatchEvents(idLeague)
                } else {
                    next_matches_swipe_refresh.isRefreshing = false
                }
            } else {
                next_matches_swipe_refresh.isRefreshing = false
            }
        }
    }

    private fun initLayout(){
        adapterRvMatchEvents = MatchAdapter(matchEvents,true,{idMatch ->
            startActivity<DetailMatch>(DetailMatch.INTENT_ID_MATCH to idMatch)
        },{matchEvent ->
            addToCalendar(matchEvent)
        })
        rv_nextmatch.layoutManager = LinearLayoutManager(ctx)
        rv_nextmatch.addItemDecoration(DividerItemDecoration(ctx, LinearLayoutManager.VERTICAL))
        rv_nextmatch.adapter = adapterRvMatchEvents
    }

    override fun loadingSpinnerData(loading: Boolean) {
       load = loading
        if(load){
            progressBar.visible()
        }else{
            progressBar.gone()
        }
    }

    override fun setSpinnerData(leagues: List<League>) {
        this.leagues.clear()
        this.leagues.addAll(leagues)

        val spinnerItems = arrayOfNulls<String>(leagues.size)
        for (i in leagues.indices) {
            spinnerItems[i] = leagues[i].name
        }

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        next_matches_spinner.adapter = spinnerAdapter
        next_matches_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                idLeague = leagues[position].id ?: ""
                if (idLeague.isNotEmpty()) presenter.loadNextMatchEvents(idLeague)
            }

        }
    }

    override fun loadingPrevMatchesData(loading: Boolean) {
       load = loading
        next_matches_swipe_refresh.isRefreshing = load
    }

    override fun setMatchEvents(matchEvents: List<MatchEvent>) {
        this.matchEvents.clear()
        this.matchEvents.addAll(matchEvents)
        adapterRvMatchEvents.notifyDataSetChanged()
    }

    override fun showToastMsg(msg: String) {
        longToast(msg)
    }

    private fun addToCalendar(matchEvent: MatchEvent) {
        val date = matchEvent.dateEvent ?: ""
        val time = matchEvent.strTime ?: "00:00"

        if (date.isNotEmpty()) {
            val startTimeInMillis = MyDateFormat.gregorianDateTimeInMillis(date, time)
            val endTimeInMillis = startTimeInMillis + TimeUnit.MINUTES.toMillis(90)

            val intent = Intent(Intent.ACTION_EDIT)
            intent.type = "vnd.android.cursor.item/event"
            intent.putExtra(CalendarContract.Events.TITLE, "${matchEvent.strFilename}")
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "${matchEvent.strHomeTeam}")
            intent.putExtra(CalendarContract.Events.DESCRIPTION, "${matchEvent.strFilename}")
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTimeInMillis)
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTimeInMillis)
            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
            intent.putExtra(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE)
            intent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
            startActivity(intent)
        } else {
            showToastMsg("Schedule not available")
        }

    }

    override fun onStop() {
        presenter.clearComposite()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.disposeComposite()
        super.onDestroy()
    }

}