package com.nurfaizin.finalkade

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.nurfaizin.finalkade.view.ui.detailmatch.DetailMatch
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MatchDetailTest {

    @Rule
    @JvmField
    val activityTestRule: ActivityTestRule<DetailMatch> =
        object : ActivityTestRule<DetailMatch>(DetailMatch::class.java) {
            override fun getActivityIntent(): Intent {
                val intent = Intent()
                intent.putExtra(DetailMatch.INTENT_ID_MATCH, "582125")
                return intent
            }
        }

    @Test
    fun testMatchDetailsBehaviour() {
        Espresso.onView(ViewMatchers.withId(R.id.match_detail_coordinator_layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.match_details_txt_date))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.match_details_txt_home_score))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.match_details_txt_away_score))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.match_details_txt_home_team))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.match_details_txt_away_team))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.match_details_menu_favorite))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Thread.sleep(500)
        Espresso.onView(ViewMatchers.withId(R.id.match_details_menu_favorite)).perform(ViewActions.click())
        Thread.sleep(1000)
        Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.snackbar_text),
                ViewMatchers.withText(Matchers.containsString("Added to Favorites"))
            )
        )
        Thread.sleep(2000)
        Espresso.onView(ViewMatchers.withId(R.id.match_details_menu_favorite)).perform(ViewActions.click())
        Thread.sleep(1000)
        Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.snackbar_text),
                ViewMatchers.withText(Matchers.containsString("Removed from favorite"))
            )
        )
    }
}