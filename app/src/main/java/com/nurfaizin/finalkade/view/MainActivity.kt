package com.nurfaizin.finalkade.view

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.nurfaizin.finalkade.R
import com.nurfaizin.finalkade.view.ui.favorites.FavoritesFragment
import com.nurfaizin.finalkade.view.ui.match.MatchFragment
import com.nurfaizin.finalkade.view.ui.team.TeamFragment
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {


    private lateinit var currentFragment: Fragment
    private lateinit var lastFragment: Fragment
    private var firstFragmentOpened = true

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup View
        setTitle(R.string.dashboard_page_title)
        setSupportActionBar(dashboard_toolbar)

        // bottom navigation listener
        dashboard_bot_nav.setOnNavigationItemSelectedListener {
            displaySelectedPage(it.itemId)
            return@setOnNavigationItemSelectedListener true
        }

        // open prev match page
        displaySelectedPage(R.id.dashboard_bot_nav_matches)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun displaySelectedPage(itemId: Int) {
        when (itemId) {
            R.id.dashboard_bot_nav_matches -> {
                currentFragment = MatchFragment()
                openFragment()
            }
            R.id.dashboard_bot_nav_teams -> {
                currentFragment = TeamFragment()
                openFragment()
            }
            R.id.dashboard_bot_nav_favorites -> {
                currentFragment = FavoritesFragment()
                openFragment()
            }
            else -> {
                currentFragment = MatchFragment()
                openFragment()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun openFragment() {
        if (firstFragmentOpened) {
            supportFragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.dashboard_frame_layout, currentFragment, currentFragment::class.java.simpleName)
                .commit()
            firstFragmentOpened = false
            lastFragment = currentFragment
        } else {
            if (!Objects.equals(currentFragment::class.java.simpleName, lastFragment::class.java.simpleName)) {
                supportFragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.dashboard_frame_layout, currentFragment, currentFragment::class.java.simpleName)
                    .addToBackStack(lastFragment::class.java.simpleName)
                    .commit()
                lastFragment = currentFragment
            }
        }
    }
}




