package com.nurfaizin.finalkade.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchMatchEventResponse (
    @SerializedName("event") @Expose val matchEvents: List<MatchEvent>
)