package com.nurfaizin.finalkade.view.ui.matchprev

import com.nurfaizin.finalkade.api.TheSportDBApi
import com.nurfaizin.finalkade.model.LeagueResponse
import com.nurfaizin.finalkade.model.MatchEventResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MatchPrevPresenter(private val view :MatchPrevContract.View,
                         private val theSportDBApiServices: TheSportDBApi) : MatchPrevContract.Presenter{



    private val composite = CompositeDisposable()

    override fun loadLeague() {

        composite.add(
            theSportDBApiServices.getListLeague()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({leaguesResponse: LeagueResponse? ->
                    view.loadingSpinnerData(false)
                    leaguesResponse?.leagues?.let {leagues ->
                        view.setSpinnerData(leagues)
                    }
                }, {
                    view.loadingSpinnerData(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }

    override fun loadPrevMatchEvents(idLeague: String) {
        view.loadingPrevMatchesData(true)

        composite.add(
            theSportDBApiServices.getPastMatchEvents(idLeague)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({matchEventsResponse: MatchEventResponse? ->
                    view.loadingPrevMatchesData(false)
                    matchEventsResponse?.events?.let {matchEvents ->
                        view.setMatchEvents(matchEvents)
                    }
                }, {
                    view.loadingPrevMatchesData(false)
                    view.showToastMsg("${it.message}")
                })
        )
    }




    override fun clearComposite() {
        composite.clear()
    }

    override fun disposeComposite() {
        composite.dispose()
    }



}